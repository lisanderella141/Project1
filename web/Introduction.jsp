<%-- 
    Document   : Introduction
    Created on : Dec 4, 2017, 11:23:23 PM
    Author     : Nazatul
--%>

<html>
    <head>
        <title>INTRODUCTION</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <style>
            body{ background-color: mistyrose;}
            div.container{ width: 80%;
                           border: 2px solid black;
                           margin:50px;
                           margin-left: 130px;
                           
            }
            header, footer{padding: 1em;
                            color:black;
                            background-color: gainsboro;
                            clear:left;
                            text-align: center;
            
            }
            header{border-bottom: 2px solid black;}
            footer{border-top: 2px solid black;}
            nav{
                float:left;
                max-width: 300px;
                margin:0;
                padding:1em;
                background-color: gainsboro;
                height: 350px;
                
            }
            nav ul {list-style-type: none;
                    padding: 0;}
            nav ul a {text-decoration: none;}

            article {
                    margin-left: 170px;
                    border-left: 2px solid black;
                    padding: 1em;
                    overflow: hidden;
                    height:350px;
                    }
            h2{text-align: Left;
            margin: 10px;}
        </style>
    </head>
    <body>
        <div class="container">
            <header>
                <h1> Versioning Application With Git</h1>
            </header>
            <nav>
                <ul>
                    <li><a href="Introduction.jsp">Introduction</a></li>
                    <li><a href="Initializing.jsp">Initializing a Git Repo</a></li>
                    <li><a href="Adding.jsp">Adding Files to a Git Repo</a></li>
                    <li><a href="Editing.jsp">Editing Files</a></li>
                    <li><a href="Committing.jsp">Commiting Sources to a Local Repo</a></li>
                    <li><a href="WBranches.jsp">Working With Branches</a></li>
                    <li><a href="WRemote.jsp">Working With Remote Repo</a></li>
                    <li><a href="Cloning.jsp">Cloning a Git Repo</a></li>
                    <li><a href="Summary.jsp">Summary</a></li>
                </ul>
            </nav>
            <article>
                <h2>INTRODUCTION</h2>
                <p>
                </p>
            </article>
            <footer>Copyright &copy; Nazatul Nurlisa Binti Zolkipli</footer>
        </div>
    </body>
</html>
